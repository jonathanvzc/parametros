package latinparametros.com.sarparameters.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import latinparametros.com.sarparameters.objects.beans.Notification;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class DatabaseManager extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "NotificationsDB";
    private static int DATABASE_VERSION = 1;

    public DatabaseManager(Context context){
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Notification.createTable(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }

    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase,
                            int oldVersion, int newVersion){
        Notification.deleteTable(sqLiteDatabase);
    }
}
