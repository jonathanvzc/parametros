package latinparametros.com.sarparameters.helpers.interfaces;

import latinparametros.com.sarparameters.objects.ws.ServerResponse;

/**
 * Created by jonathan.zepeda on 20/03/2018
 */

public interface DialogInterface {
    void ejectAction();
    void processAction(ServerResponse serverResponse);
}
