package latinparametros.com.sarparameters.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import latinparametros.com.sarparameters.R;
import latinparametros.com.sarparameters.helpers.interfaces.DialogInterface;
import latinparametros.com.sarparameters.helpers.interfaces.ObjectTreatment;
import latinparametros.com.sarparameters.objects.ws.ServerResponse;
import latinparametros.com.sarparameters.objects.ws.response.BaseResponse;
import latinparametros.com.sarparameters.ws.LoginClient;

public class LoginActivity extends AppCompatActivity implements DialogInterface {
    private ProgressDialog progressDialog;
    private BaseResponse getBaseResponse;
    EditText editTextUser;
    EditText editTextPass;

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R
                .layout.activity_login);
        this.setTitle(R.string.title_activity_login);
        initComponents();
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void initComponents(){
        Button boton = (Button) findViewById(R.id.btnLogin);
        editTextUser = findViewById(R.id.txtUsuario);
        editTextPass = findViewById(R.id.txtPass);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String usuario = editTextUser.getText().toString();
                String pass = editTextPass.getText().toString();

                    if(isEmailValid(usuario)){
                        if(TextUtils.isEmpty(pass)){
                            editTextPass.setError("Ingrese clave");
                            return;
                        }else {
                            if (!isOnline()){
                                new AlertDialog.Builder(LoginActivity.this).setMessage("necesita internet").setPositiveButton("OK", new android.content.DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(android.content.DialogInterface dialogInterface, int i) {

                                    }
                                }).create().show();
                            }else {
                                progressDialog = ProgressDialog.show(LoginActivity.this, "", "Verificando...");
                                LoginClient loginClient = new LoginClient(LoginActivity.this, LoginActivity.this);
                                loginClient.execute(usuario, pass);
                            }
                        }
                    }else{
                        editTextUser.setError("Ingrese un E-mail");
                        return;
                    }

            }
        });
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void ejectAction() {
    }

    @Override
    public void processAction(ServerResponse serverResponse) {
        progressDialog.cancel();

        if(serverResponse.isSucessful()){
            getBaseResponse = ObjectTreatment.parseStrToObject(serverResponse.getJson(), BaseResponse.class);
            if(getBaseResponse.isSucessful()){
                startActivity(new Intent(this, ParametersActivity.class));
            }else{
                new AlertDialog.Builder(this).setMessage(getBaseResponse.getMessage()).setPositiveButton("Ok", new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(android.content.DialogInterface dialogInterface, int i) {

                    }
                }).create().show();
            }
        }
    }
}
