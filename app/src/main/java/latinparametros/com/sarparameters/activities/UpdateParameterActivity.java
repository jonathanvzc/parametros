package latinparametros.com.sarparameters.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import latinparametros.com.sarparameters.R;
import latinparametros.com.sarparameters.helpers.interfaces.DialogInterface;
import latinparametros.com.sarparameters.objects.beans.Parameter;
import latinparametros.com.sarparameters.objects.parcelable.ParameterParcelable;
import latinparametros.com.sarparameters.objects.ws.ServerResponse;
import latinparametros.com.sarparameters.ws.ParameterUpdateClient;

public class UpdateParameterActivity extends AppCompatActivity implements DialogInterface {
    private ParameterParcelable parameter;
    private ProgressDialog progressDialog;
    private TextView tvCodigo;
    private TextView tvNoCia;
    private TextView tvNombre;
    private TextView tvDato;
    private Button btnUpdate;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_parameter);
        this.setTitle(R.string.title_activity_update_paremeter);
       initComponents();
    }

    public void initComponents(){
        btnUpdate = findViewById(R.id.btnUpdateParam);
        tvCodigo = findViewById(R.id.tv_frm_code);
        tvNoCia = findViewById(R.id.tv_frm_nocia);
        tvNombre = findViewById(R.id.tv_frm_nombre);
        tvDato = findViewById(R.id.tv_frm_dato);

        parameter = getIntent().getParcelableExtra(
                ParametersActivity.PARAMETRO_PARAM);
        position = getIntent().getIntExtra("position",0);

        tvCodigo.setText(parameter.getCodigo());
        tvNoCia.setText(parameter.getNoCia());
        tvNombre.setText(parameter.getNombre());
        tvDato.setText(parameter.getDato());

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Parameter parameterUpdate = new Parameter();
                parameterUpdate.setCodigo(parameter.getCodigo());
                parameterUpdate.setNoCia(parameter.getNoCia());
                parameterUpdate.setNombre(parameter.getNombre());
                parameterUpdate.setDato(tvDato.getText().toString());
                parameter.setDato(tvDato.getText().toString());

                progressDialog = ProgressDialog.show(UpdateParameterActivity.this, "", "En proceso...");

                ParameterUpdateClient loginClient = new ParameterUpdateClient(UpdateParameterActivity.this, UpdateParameterActivity.this);
                loginClient.execute(parameterUpdate);

            }
        });

    }

    @Override
    public void ejectAction() {

    }

    @Override
    public void processAction(ServerResponse serverResponse) {
        progressDialog.cancel();
        if(serverResponse.isSucessful()){
            Toast.makeText(UpdateParameterActivity.this, "Parametro actualizado correctamente", Toast.LENGTH_LONG).show();

            Intent resultIntent = new Intent();
            resultIntent.putExtra(ParametersActivity.PARAMETRO_PARAM,parameter);
            resultIntent.putExtra("position",position);
            setResult(RESULT_OK,resultIntent);
            finish();
        }
    }
}
