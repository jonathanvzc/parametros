package latinparametros.com.sarparameters.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import latinparametros.com.sarparameters.R;

public class SplashScreenActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 5_000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(runParameterList, SPLASH_TIME_OUT);
    }

    private Runnable runParameterList = new Runnable() {
        @Override
        public void run() {
            goToParameterList();
        }
    };


    private void goToParameterList(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
