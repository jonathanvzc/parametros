package latinparametros.com.sarparameters.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import latinparametros.com.sarparameters.R;
import latinparametros.com.sarparameters.objects.adapters.NotificationAdapterRecycler;
import latinparametros.com.sarparameters.objects.beans.Notification;

public class NotificationsActivity extends AppCompatActivity {
    private RecyclerView rv_list_notifications;
    private List notificationList = new ArrayList<Notification>();
    private NotificationAdapterRecycler adapter;
    private GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        this.setTitle(R.string.title_activity_notifications);
        rv_list_notifications = (RecyclerView) findViewById(R.id.rv_notifications);
        gridLayoutManager = new GridLayoutManager(this,1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initComponents();
    }

    public void initComponents(){
        rv_list_notifications.setLayoutManager(gridLayoutManager);
        notificationList = Notification.findAll(this);
        Log.i("Notification", "size "+notificationList.size());
        adapter = new NotificationAdapterRecycler(notificationList, this, R.layout.item_notification);
        rv_list_notifications.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }
}
