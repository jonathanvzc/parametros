package latinparametros.com.sarparameters.activities;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.support.v7.widget.SearchView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;
import java.util.List;

import latinparametros.com.sarparameters.R;
import latinparametros.com.sarparameters.fcm.RegistrationIntentService;
import latinparametros.com.sarparameters.helpers.interfaces.DialogInterface;
import latinparametros.com.sarparameters.helpers.interfaces.ObjectTreatment;
import latinparametros.com.sarparameters.objects.adapters.ParameterListAdapter;
import latinparametros.com.sarparameters.objects.beans.Parameter;
import latinparametros.com.sarparameters.objects.parcelable.ParameterParcelable;
import latinparametros.com.sarparameters.objects.ws.ServerResponse;
import latinparametros.com.sarparameters.objects.ws.response.BaseResponse;
import latinparametros.com.sarparameters.objects.ws.response.GetParametersResponse;
import latinparametros.com.sarparameters.ws.ParameterListClient;

public class ParametersActivity extends AppCompatActivity implements DialogInterface,SearchView.OnQueryTextListener {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private final int NEW_PARAMETER_CODE = 123;
    public static final String PARAMETRO_PARAM = "paramKey";
    private ProgressDialog progressDialog;
    private GetParametersResponse getParametersResponse;
    private BaseResponse getBaseResponse;
    private List parameterList = new ArrayList<Parameter>();
    ArrayList<Parameter> myListItems  = new ArrayList<Parameter>();
    ParameterListAdapter adapter;
    private RecyclerView rvListParam;
    private ListView listView;

    private SearchView searchView;
    private MenuItem searchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parameters);
        this.setTitle(R.string.title_activity_parameter_list);
        if(checkPlayServices()) {
            startService(new Intent(ParametersActivity.this, RegistrationIntentService.class));
        }
        initParameters();

    }

    public void initParameters(){
        progressDialog = ProgressDialog.show(ParametersActivity.this, "Cargando de datos en proceso", "Por favor espere...");
        ParameterListClient paramListClient = new ParameterListClient(ParametersActivity.this, ParametersActivity.this);
        paramListClient.execute();
        rvListParam = (RecyclerView)findViewById(R.id.rv_list_param);
        listView = findViewById(R.id.lv_param);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);
        rvListParam.setLayoutManager(gridLayoutManager);

        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Parameter param = (Parameter) listView.getItemAtPosition(position);
                ParameterParcelable parameterParcelable = new ParameterParcelable();
                parameterParcelable.setNoCia(param.getNoCia());
                parameterParcelable.setCodigo(param.getCodigo());
                parameterParcelable.setNombre(param.getNombre());
                parameterParcelable.setDato(param.getDato());
                Intent intent = new Intent(new Intent(ParametersActivity.this, UpdateParameterActivity.class));
                intent.putExtra(PARAMETRO_PARAM,parameterParcelable);
                intent.putExtra("position", position);
                startActivityForResult(intent,NEW_PARAMETER_CODE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.parameters_menu_layout,menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.menu_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notifications:
                startActivity(new Intent(this, NotificationsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void ejectAction() {

    }

    @Override
    public void processAction(ServerResponse serverResponse) {
        progressDialog.cancel();
        if(serverResponse.isSucessful()){
            getParametersResponse = ObjectTreatment.parseStrToObject(serverResponse.getJson(),GetParametersResponse.class);
            if(getParametersResponse.isSucessful()){
                parameterList = getParametersResponse.getParameterList();
                myListItems.addAll(parameterList);
                Log.i("Cantidad" , " : "+ getParametersResponse.getParameterList().size());
                adapter = new ParameterListAdapter(this, myListItems);
                // Attach the adapter to a ListView
                ListView listView = (ListView) findViewById(R.id.lv_param);
                listView.setAdapter(adapter);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        int position;
        if((requestCode == NEW_PARAMETER_CODE)&& (resultCode == RESULT_OK)&& (data != null)){
            ParameterParcelable parameterParcelable = (ParameterParcelable)data.getParcelableExtra(PARAMETRO_PARAM);
            Parameter newParameter = new Parameter();
            newParameter.setNoCia(parameterParcelable.getNoCia());
            newParameter.setCodigo(parameterParcelable.getCodigo());
            newParameter.setNombre(parameterParcelable.getNombre());
            newParameter.setDato(parameterParcelable.getDato());
            adapter.clear();
            position = data.getIntExtra("position",0);
            parameterList.set(position,newParameter);
            myListItems.clear();
            myListItems.addAll(parameterList);
            adapter.addAll(parameterList);
        }
    }

    private boolean checkPlayServices(){
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if(resultCode != ConnectionResult.SUCCESS){
            if(apiAvailability.isUserResolvableError(resultCode)){
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else{
                Log.i("", "Dispositivo no es compatible");
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        Log.i("Text","Text Change");
        return true;
    }
}
