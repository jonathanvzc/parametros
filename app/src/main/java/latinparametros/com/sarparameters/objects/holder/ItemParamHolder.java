package latinparametros.com.sarparameters.objects.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class ItemParamHolder extends RecyclerView.ViewHolder {
    private CardView cv_container;
    private TextView tvCode;
    private TextView tvNombre;

    public ItemParamHolder(View itemView) {
        super(itemView);

    }

    public CardView getCv_container() {
        return cv_container;
    }

    public void setCv_container(CardView cv_container) {
        this.cv_container = cv_container;
    }

    public TextView getTvCode() {
        return tvCode;
    }

    public void setTvCode(TextView tvCode) {
        this.tvCode = tvCode;
    }

    public TextView getTvNombre() {
        return tvNombre;
    }

    public void setTvNombre(TextView tvNombre) {
        this.tvNombre = tvNombre;
    }
}
