package latinparametros.com.sarparameters.objects.beans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import latinparametros.com.sarparameters.database.DatabaseManager;
import latinparametros.com.sarparameters.database.Model;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class Notification extends Model {
    public static final String TABLE_NAME = "Notification";

    private Long id;
    private static final String FIELD_ID = "id";

    private String message;
    private static final String FIELD_MESSAGE = "message";

    private String date;
    private static final String FIELD_DATE = "date";

    public Notification() {
    }

    public static void createTable(SQLiteDatabase sqLiteDatabase) {
        String sqlCreate = SQL_CREATE_TABLE + TABLE_NAME + " (" +
                FIELD_ID + SQL_AUTOINCREMENT_ID +
                FIELD_MESSAGE + SQL_TYPE_TEXT + SQL_SEP +
                FIELD_DATE + SQL_TYPE_DATE + ");";
        Log.i("SQL Create", sqlCreate);
        sqLiteDatabase.execSQL(sqlCreate);

    }

    public Notification(Long id, String message, String date) {
        this.id = id;
        this.message = message;
        this.date = date;
    }

    public static List<Notification> findAll(Context context) {
        return find(context, null, null);
    }

    public static List<Notification> find(Context context,
                                     String whereClause,
                                     String[] whereArgs) {
        List<Notification> result = new ArrayList<>();

        DatabaseManager databaseManager =
                new DatabaseManager(context);
        SQLiteDatabase sqLiteDatabase =
                databaseManager.getReadableDatabase();

        String[] requiredFields = {
                FIELD_ID,
                FIELD_MESSAGE,
                FIELD_DATE
        };
        Cursor cursor = sqLiteDatabase.query(
                TABLE_NAME,
                requiredFields,
                whereClause,
                whereArgs,
                null, null, null, null
        );

        if (cursor.moveToFirst()) {
            Notification notification;

            do {
                notification = new Notification(cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2));
                Log.i("SQL Find", ""+cursor.getLong(0));
                result.add(notification);
            } while (cursor.moveToNext());
        }

        sqLiteDatabase.close();
        databaseManager.close();

        return result;
    }

    @Override
    public boolean save(Context context) {
        boolean result = false;

        DatabaseManager databaseManager = new DatabaseManager(context);
        SQLiteDatabase sqLiteDatabase = databaseManager.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(FIELD_ID, this.id);
        cv.put(FIELD_MESSAGE, this.message);
        cv.put(FIELD_DATE, this.date);

        //update
        if (id != null) {
            String whereClause = FIELD_ID + "= ?";
            String[] whereArgs = {id.toString()};
            result = (sqLiteDatabase.update(TABLE_NAME, cv,
                    whereClause, whereArgs) > 0);
            //Create
        } else {
            id = sqLiteDatabase.insert(TABLE_NAME,
                    null, cv);
            result = (id > 0);

            if(id != -1)
                Log.d("SAVE","New row added, row id: " + id);
            else
                Log.d("SAVE","Something wrong");
        }

        sqLiteDatabase.close();
        databaseManager.close();

        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
