package latinparametros.com.sarparameters.objects.beans;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class Parameter {
    @JsonProperty(value="NO_CIA")
    private String noCia;

    @JsonProperty(value="COD_PARAMETRO")
    private String codigo;

    @JsonProperty(value="DATO")
    private String dato;

    @JsonProperty(value="NOMBRE")
    private String nombre;

    public Parameter() {
    }

    public String getNoCia() {
        return noCia;
    }

    public void setNoCia(String noCia) {
        this.noCia = noCia;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
