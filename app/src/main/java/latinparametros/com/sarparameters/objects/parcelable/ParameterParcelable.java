package latinparametros.com.sarparameters.objects.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class ParameterParcelable implements Parcelable {
    private String noCia;
    private String codigo;
    private String nombre;
    private String dato;

    protected ParameterParcelable(Parcel in) {
        noCia = in.readString();
        codigo = in.readString();
        nombre = in.readString();
        dato = in.readString();
    }

    public ParameterParcelable() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(noCia);
        dest.writeString(codigo);
        dest.writeString(nombre);
        dest.writeString(dato);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ParameterParcelable> CREATOR = new Parcelable.Creator<ParameterParcelable>() {
        @Override
        public ParameterParcelable createFromParcel(Parcel in) {
            return new ParameterParcelable(in);
        }

        @Override
        public ParameterParcelable[] newArray(int size) {
            return new ParameterParcelable[size];
        }
    };

    public String getNoCia() {
        return noCia;
    }

    public void setNoCia(String noCia) {
        this.noCia = noCia;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }
}