package latinparametros.com.sarparameters.objects.beans;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class Login {
    @JsonProperty(value="LOGIN",required = false,defaultValue = "No Name")
    String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
