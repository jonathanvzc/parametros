package latinparametros.com.sarparameters.objects.ws.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */
public class GetLoginResponse extends BaseResponse {
    @JsonProperty(value="resp")
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
