package latinparametros.com.sarparameters.objects.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import latinparametros.com.sarparameters.R;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class NotificationHolder extends RecyclerView.ViewHolder {
    private CardView cv_container_notification;
    private TextView tv_message;
    private TextView tv_date;

    public NotificationHolder(View itemView){
        super(itemView);
        cv_container_notification = itemView.findViewById(R.id.cv_container_notification);
        this.tv_message = itemView.findViewById(R.id.txtMessage);
        this.tv_date = itemView.findViewById(R.id.tv_date_notification);
    }

    public CardView getCv_container_notification() {
        return cv_container_notification;
    }

    public void setCv_container_notification(CardView cv_container_notification) {
        this.cv_container_notification = cv_container_notification;
    }

    public TextView getTv_message() {
        return tv_message;
    }

    public void setTv_message(TextView tv_message) {
        this.tv_message = tv_message;
    }

    public TextView getTv_date() {
        return tv_date;
    }

    public void setTv_date(TextView tv_date) {
        this.tv_date = tv_date;
    }
}
