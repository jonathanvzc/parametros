package latinparametros.com.sarparameters.objects.ws.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse {

    @JsonProperty(value = "codigo")
    private String codigo;

    @JsonProperty(value = "mensaje")
    private String message;

    private boolean sucessful;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSucessful() {
        return sucessful;
    }

    public void setSucessful(boolean sucessful) {
        this.sucessful = sucessful;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
        this.sucessful = (codigo.equalsIgnoreCase("0000"));
    }
}
