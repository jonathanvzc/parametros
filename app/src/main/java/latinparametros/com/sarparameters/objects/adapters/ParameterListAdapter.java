package latinparametros.com.sarparameters.objects.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import latinparametros.com.sarparameters.R;
import latinparametros.com.sarparameters.objects.beans.Parameter;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class ParameterListAdapter extends ArrayAdapter<Parameter> implements Filterable {
    private List<Parameter> data = new ArrayList<>();
        public ParameterListAdapter(Context context, ArrayList<Parameter> parameters) {
            super(context, 0, parameters);
        }

        public void addAll(List<Parameter> data) {
            this.data.addAll(data);
            this.notifyDataSetChanged();
        }

        public void clear() {
            data.clear();
            this.notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            Parameter parameter = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_parameter_list, parent, false);
            }
            // Lookup view for data population
            TextView tvItemNoCia = (TextView) convertView.findViewById(R.id.txt_item_nocia);
            TextView tvItemCode = (TextView) convertView.findViewById(R.id.txt_item_codigo);
            TextView tvItemName = (TextView) convertView.findViewById(R.id.txt_item_nombre);
            // Populate the data into the template view using the data object
            tvItemNoCia.setText(parameter.getNoCia());
            tvItemCode.setText(parameter.getCodigo());
            tvItemName.setText(parameter.getNombre());

            if (position % 2 == 1) {
                convertView.setBackgroundResource(R.color.listViewBack);
            } else {
                convertView.setBackgroundColor(Color.WHITE);
            }
            // Return the completed view to render on screen
            return convertView;
        }


}
