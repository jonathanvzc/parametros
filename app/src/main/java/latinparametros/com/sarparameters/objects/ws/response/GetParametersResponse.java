package latinparametros.com.sarparameters.objects.ws.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import latinparametros.com.sarparameters.objects.beans.Parameter;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class GetParametersResponse extends BaseResponse {
    @JsonProperty(value="resp")
    private List<Parameter> parameterList;

    public List<Parameter> getParameterList() {
        return parameterList;
    }

    public void setParameterList(List<Parameter> parameterList) {
        this.parameterList = parameterList;
    }
}
