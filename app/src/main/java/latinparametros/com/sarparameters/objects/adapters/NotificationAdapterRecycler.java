package latinparametros.com.sarparameters.objects.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import latinparametros.com.sarparameters.objects.beans.Notification;
import latinparametros.com.sarparameters.objects.holder.NotificationHolder;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class NotificationAdapterRecycler extends RecyclerView.Adapter<NotificationHolder> {
    private List<Notification> notificationList;
    private Activity activity;
    private int resourceLayout;

    public NotificationAdapterRecycler(List<Notification> notificationList, Activity activity, int resourceLayout) {
        this.notificationList = notificationList;
        this.activity = activity;
        this.resourceLayout = resourceLayout;
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationHolder(LayoutInflater.from(activity).inflate(resourceLayout,parent,false));
    }

    @Override
    public void onBindViewHolder(NotificationHolder holder, int position) {
        final Notification notification = notificationList.get(position);
        holder.getTv_message().setText(notification.getMessage().toString());
        holder.getTv_date().setText(notification.getDate());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }
}
