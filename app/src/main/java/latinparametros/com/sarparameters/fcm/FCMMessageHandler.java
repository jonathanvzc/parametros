package latinparametros.com.sarparameters.fcm;

import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Date;
import java.util.Map;


import latinparametros.com.sarparameters.R;
import latinparametros.com.sarparameters.objects.beans.Notification;

/**
 * Created by jonathan.zepeda on 20/03/2018
 */

public class FCMMessageHandler extends FirebaseMessagingService {
    public static final int MESSAGE_NOTIFICATION_ID = 435345;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Notification notificationS = new Notification();
        long timestamp=System.currentTimeMillis();
        Map<String, String> data = remoteMessage.getData();
        String from = remoteMessage.getFrom();
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        Log.d("push", "From: " + from);
        Log.d("push", "Message body: " + notification.getBody().toString());
        notificationS.setMessage(notification.getBody().toString());
        notificationS.setDate(java.text.DateFormat.getDateTimeInstance().format(new Date(timestamp)));
        notificationS.save(this);
        createNotification(notification);
    }

    private void createNotification(RemoteMessage.Notification notification){
        Context context = getBaseContext();
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(uri)
                .setContentTitle(notification.getTitle()).setContentText(notification.getBody());
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID,mBuilder.build());

    }
}
