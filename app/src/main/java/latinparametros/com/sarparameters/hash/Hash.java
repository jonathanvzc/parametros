package latinparametros.com.sarparameters.hash;

/**
 * Created by jonathan.zepeda on  20/03/2018
 */

import android.util.Base64;
import android.util.Log;

import java.net.URLEncoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import latinparametros.com.sarparameters.BuildConfig;

public class Hash {

    public String hashSHA256(String string){
        try{
            byte ptext[] = string.getBytes();
            string = new String(ptext, "ISO-8859-1");

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(BuildConfig.SECRET_KEY.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            String hash = Base64.encodeToString(sha256_HMAC.doFinal(string.getBytes()), Base64.DEFAULT);
            hash = hash.replace("\n", "");
            String hashUrlEncoded = URLEncoder.encode(hash);
            return hashUrlEncoded;
        }catch (Exception e){
            Log.e( "Hash","Error in hash generation . "  + e.getMessage());
        }
        return null;
    }
}
