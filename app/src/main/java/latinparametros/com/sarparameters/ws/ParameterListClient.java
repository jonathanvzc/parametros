package latinparametros.com.sarparameters.ws;

import android.content.Context;
import android.os.AsyncTask;

import java.net.URLEncoder;

import latinparametros.com.sarparameters.hash.Hash;
import latinparametros.com.sarparameters.helpers.interfaces.DialogInterface;
import latinparametros.com.sarparameters.objects.ws.ServerResponse;

/**
 * Created by jonathan.zepeda on 20/03/2018.
 */

public class ParameterListClient extends AsyncTask<String,Void,ServerResponse> {
    private String ENDPOINT="https://www.mcd.com.gt/WSapp/rest/gw/exec/?" +
            "noCia=40&" +
            "appId=7b52009b64fd0a2a49e6d8a939753077792b0554&"+
            "version=1.0.0&";
    private Context context;
    private DialogInterface dialogInterface;

    public ParameterListClient(Context context, DialogInterface dialogInterface) {
        this.context = context;
        this.dialogInterface = dialogInterface;
    }

    @Override
    protected ServerResponse doInBackground(String... strings) {
        String webService = "{\"webservice\":\"Parametros\"}";
        ENDPOINT += "jSonParam=" + URLEncoder.encode(webService);
        Hash hash = new Hash();
        ENDPOINT += "&hash=" + hash.hashSHA256(webService);
        return ServerClient.connect(context, ENDPOINT, null);
    }

    @Override
    protected void onPostExecute(ServerResponse serverResponse) {
        dialogInterface.processAction(serverResponse);

    }
}
